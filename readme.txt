=== Innovage Export Users ===
Contributors: SinOB
Requires at least: 4.1
Tested up to: 4.1
Stable tag: 1.2
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Allow admin user to export user and step data to csv.


== Description ==

Allow an administrator user to export user and step data to a csv file. Allows
extraction for a specific date range and filtering on group membership.
There are currently no admin options.


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
