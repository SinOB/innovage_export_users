<?php

/**
  Plugin Name: Innovage Export Users
  Plugin URI: https://bitbucket.org/SinOB/innovage_fpdf
  Description: Allow admin user to export user and step data to csv.
  Author: Sinead O'Brien
  Version: 1.2
  Author URI: https://bitbucket.org/SinOB
  Requires at least: 4.1
  Tested up to: 4.1
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */
/* Heavily based on plugin Export Users to CSV by Ulrich Sossou
  (http://github.com/sorich87)
 */

class Innovage_Export_Users {

    /** /
     * Constructor
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_admin_pages'));
        add_action('init', array($this, 'generate_csv'));
        add_action('admin_footer', array($this, 'export_users_scripts'));
        add_action("admin_enqueue_scripts", array($this, 'enqueue_styles_scripts'));
    }

    /** /
     * Add admin menu
     */
    public function add_admin_pages() {
        add_users_page('Export to CSV', 'Export to CSV', 'list_users', 'export-users-to-csv', array($this, 'users_page'));
    }

    /** /
     * Embed the necessary javascript and css scripts on a page
     * if they have not already been included.
     */
    function enqueue_styles_scripts() {
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_style('jquery-ui-css', plugins_url('css/jquery-ui.css', __FILE__));
    }

    /** /
     * Enable start and end date fields to have jquery datepicker
     */
    public function export_users_scripts() {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('.custom_export_date').datepicker({
                    maxDate: new Date,
                    dateFormat: 'yy-mm-dd',
                    showOn: "both",
                    buttonImageOnly: true,
                    buttonImage: "<?php echo plugins_url('images/calendar_icon.png', __FILE__) ?>"
                });
            });
        </script>
        <?php
    }

    /** /
     * Generate the CSV file
     * @global type $wpdb
     * @return type
     */
    public function generate_csv() {
        // User must have admin permission
        if (!is_admin()) {
            return;
        }

        // User must be on export-users-to-csv page
        $needle = 'users.php?page=export-users-to-csv';
        if (substr($_SERVER['REQUEST_URI'], -strlen($needle)) != $needle) {
            return;
        }

        // Make sure current user is logged in
        if (!is_user_logged_in()) {
            return;
        }

        // Make sure current user has list user permissions
        if (!current_user_can('list_users')) {
            return;
        }

        if (isset($_POST['_wpnonce-innovage-export-users-users-page_export'])) {
            check_admin_referer('innovage-export-users-users-page_export', '_wpnonce-innovage-export-users-users-page_export');
            $date_format = "/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/";

            if (!isset($_POST['start_date']) || empty($_POST['start_date']) || !preg_match($date_format, $_POST['start_date'])) {
                echo '<div id="message" class="error"><p><strong>The from date is a required field and must be in the format YYYY-MM-DD.</strong></p></div>';
                return;
            }
            if (!isset($_POST['end_date']) || empty($_POST['end_date']) || !preg_match($date_format, $_POST['end_date'])) {
                echo '<div id="message" class="error"><p><strong>The to date is a required field and must be in the format YYYY-MM-DD.</strong></p></div>';
                return;
            }
            if (!isset($_POST['filter_groups']) || empty($_POST['filter_groups'])) {
                echo '<div id="message" class="error"><p><strong>The to group filter is a required field.</strong></p></div>';
                return;
            }

            // Sanitize user input
            $filter_groups = array_filter($_POST['filter_groups'], 'ctype_digit');
            $start_date = sanitize_text_field($_POST['start_date']);
            $end_date = sanitize_text_field($_POST['end_date']);

            $sitename = sanitize_key(get_bloginfo('name'));
            if (!empty($sitename)) {
                $sitename .= '.';
            }
            $filename = $sitename . 'users.' . date('Y-m-d-H-i-s') . '.csv';
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Content-Type: text/csv; charset=' . get_option('blog_charset'), true);

            global $wpdb;
            $table_name = $wpdb->prefix . "innovage_pedometer";

            $users = get_users();

            $headers = array('User ID', 'Buddypress Display name', 'Username', 'First name', 'Last name', 'Email address', 'Year of birth', 'Gender', 'Registered date', 'Steps date', 'Step count', 'Member of Group Challenge(s)');
            echo implode(',', $headers) . "\n";

            $query = "SELECT step_date, step_count"
                    . " FROM $table_name"
                    . " WHERE id= %s "
                    . " AND step_date >= %s"
                    . " AND step_date < %s"
                    . " ORDER by step_date DESC";

            $date_range = innovage_export_users_date_range($start_date, $end_date);
            foreach ($users as $user) {

                if (!$this->user_in_groups($user->ID, $filter_groups)) {
                    continue;
                }

                // Get steps for certain user between certain dates
                $sql = $wpdb->prepare($query, $user->ID, date('Y-m-d', strtotime($start_date)), date('Y-m-d', strtotime('+1 month', strtotime($end_date))));
                $results = $wpdb->get_results($sql);
                $user_groups = $this->get_group_challenges_by_user($user->ID);
                foreach ($date_range as $date) {
                    $data = $this->basic_user_info($user->ID);
                    $data[] = $date;
                    $data[] = $this->get_step_count($date, $results);
                    $data[] = $user_groups;
                    echo implode(',', $data) . "\n";
                }
            }

            exit;
        }
    }

    /** /
     * Check if user is a member of the specified filter group (or no groups).
     * Return true on success.
     * @param type $user_id
     * @param type $groups
     * @return boolean
     */
    public function user_in_groups($user_id, $groups) {
        foreach ($groups as $group_id) {
            // If user is member
            if (groups_is_user_member($user_id, $group_id)) {
                return true;
            }
            // If filtering on no group and user not a member of any group
            if ($group_id === "0" && $this->get_group_challenges_by_user($user_id) === '') {
                return true;
            }
        }
        return false;
    }

    /** /
     * Return a string of all groups that the user is a member of
     * @param type $user_id
     * @return type
     */
    public function get_group_challenges_by_user($user_id) {
        $group_ids = groups_get_user_groups($user_id);
        $groups = array();
        foreach ($group_ids["groups"] as $id) {
            $group = groups_get_group(array('group_id' => $id));
            $groups[] = $group->name;
        }
        return implode(' : ', $groups);
    }

    /** /
     * Work through all steps for specific user and return step count 
     * for a specific day
     * @param type $date
     * @param type $results
     * @return int
     */
    public function get_step_count($date, $results) {
        if (isset($results) && !empty($results)) {
            foreach ($results as $result) {
                if (substr($result->step_date, 0, 10) == $date) {
                    return $result->step_count;
                }
            }
        }
        return 0;
    }

    /** /
     * Display the export users form on the admin page
     */
    public function users_page() {
        // Make sure current user is logged in
        if (!is_user_logged_in()) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'export-users-to-csv'));
        }

        // Make sure current user has list user permissions
        if (!current_user_can('list_users')) {
            wp_die(__('You do not have sufficient permissions to access this page.', 'export-users-to-csv'));
        }

        $all_groups = $groups = BP_Groups_Group::get(array(
                    'type' => 'alphabetical',
                    'per_page' => 999,
                    'show_hidden' => true,
        ));
        ?>
        <h2>Export users to a CSV file</h2>
        <p>Export to CSV a list of users and their steps within the specified time range filtered by the specified group membership.</p>
        <form method="post" action="" enctype="multipart/form-data">
            <?php wp_nonce_field('innovage-export-users-users-page_export', '_wpnonce-innovage-export-users-users-page_export'); ?>

            <p>
                <label>Extract user steps from date in the format YYYY-MM-DD *</label><br/>
                <input type="text"
                       name="start_date"
                       id="start_date"
                       class="custom_export_date"
                       required="required" >
            </p>
            <p>
                <label>Extract user steps to date in the format YYYY-MM-DD *</label><br/>
                <input type="text"
                       name="end_date"
                       id="end_date"
                       class="custom_export_date"
                       required="required" > 
            </p>
            <p>
                Limit to users in selected groups *<br/>
                <select multiple name='filter_groups[]' size='5' required="required">
                    <?php foreach ($all_groups["groups"] as $group): ?>
                        <option value="<?php echo $group->id; ?>" ><?php echo $group->name; ?></option>
                    <?php endforeach; ?>
                    <option value="0">No group</option>
                </select>
            </p>
            <input type="submit" class="button-primary" value="Export" />
        </form>
        <?php
    }

    /** /
     * Add basic user info to CSV
     * @param type $user_id
     * @return type
     */
    public function basic_user_info($user_id) {
        $user_info = get_userdata($user_id);
        $data = array();
        $data[] = $user_id;
        $data[] = bp_core_get_username($user_id);
        $data[] = $user_info->user_login;
        $data[] = $user_info->first_name;
        $data[] = $user_info->last_name;
        $data[] = $user_info->user_email;
        $data[] = bp_get_profile_field_data(array('field' => 'Year of birth', 'user_id' => $user_id));
        $data[] = bp_get_profile_field_data(array('field' => 'Gender', 'user_id' => $user_id));
        $data[] = $user_info->user_registered;
        return $data;
    }

}

/** /
 * Generic function to return an array of dates for a specified date range
 * @param type $first
 * @param type $last
 * @param type $step
 * @param type $output_format
 * @return type
 */
function innovage_export_users_date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d') {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while ($current <= $last) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}

new Innovage_Export_Users;
